//
//  PuzzleImage.swift
//  DiplomnaRabota
//
//  Created by Stanislav Georgiev on 3.04.18.
//  Copyright © 2018 Stanislav Georgiev. All rights reserved.
//

import UIKit

class PuzzleImage: UIImage {
    
    private(set) var position: String!
    
    class func fromFile(named name: String, positioned position: String) -> PuzzleImage? {
        let fullName = name + position
        let cgImage = UIImage(named: fullName)?.cgImage
        if let validName = cgImage {
            let image = PuzzleImage(cgImage: validName)
            image.position = position
            return image
        }
        return nil
    }
    
}
