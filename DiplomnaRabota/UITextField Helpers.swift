//
//  UITextField Helpers.swift
//  DiplomnaRabota
//
//  Created by Stanislav Georgiev on 3.05.18.
//  Copyright © 2018 Stanislav Georgiev. All rights reserved.
//

import UIKit

extension UITextField {
    var safeText: String {
        return self.text ?? ""
    }
}
