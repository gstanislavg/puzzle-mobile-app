//
//  Puzzle.swift
//  DiplomnaRabota
//
//  Created by Stanislav Georgiev on 2.05.18.
//  Copyright © 2018 Stanislav Georgiev. All rights reserved.
//

import UIKit

class Puzzle: NSObject {
    
    // MARK: Properties
    
    private(set) var id      : Int?
    private(set) var name    : String
    private(set) var image   : UIImage
    private(set) var rows    : Int
    private(set) var columns : Int
    
    init(id: Int? = nil, name: String, image: UIImage, rows: Int, columns: Int) {
        self.id      = id
        self.name    = name
        self.image   = image
        self.rows    = rows
        self.columns = columns
    }
}
