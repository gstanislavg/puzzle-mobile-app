//
//  PuzzlePreviewTableViewCell.swift
//  DiplomnaRabota
//
//  Created by Stanislav Georgiev on 3.05.18.
//  Copyright © 2018 Stanislav Georgiev. All rights reserved.
//

import UIKit

class PuzzlePreviewTableViewCell: UITableViewCell {
    
    
    // MARK: Outlets
    
    @IBOutlet weak var puzzlePreviewImageView   : UIImageView!
    @IBOutlet weak var selectArrowImageView     : UIImageView!
    
    @IBOutlet weak var puzzleNameLabel          : UILabel!
    @IBOutlet weak var puzzleRowsNumberLabel    : UILabel!
    @IBOutlet weak var puzzleColumnsNumberLabel : UILabel!
    
    func setCellData(from puzzle: Puzzle) {
        self.backgroundColor = UIColor(patternImage: #imageLiteral(resourceName: "blue"))
        self.puzzlePreviewImageView.image  = puzzle.image
        self.puzzleNameLabel.text          = puzzle.name
        self.puzzleRowsNumberLabel.text    = "\(Strings.Labels.rows) \(puzzle.rows)"
        self.puzzleColumnsNumberLabel.text = "\(Strings.Labels.columns) \(puzzle.columns)"
    }
    
}
