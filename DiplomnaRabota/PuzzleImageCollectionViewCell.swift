//
//  PuzzleImageCollectionViewCell.swift
//  DiplomnaRabota
//
//  Created by Stanislav Georgiev on 20.03.18.
//  Copyright © 2018 Stanislav Georgiev. All rights reserved.
//

import UIKit

class PuzzleImageCollectionViewCell: UICollectionViewCell {
    
    
    // MARK: Outlets
    
    @IBOutlet weak var puzzlePieceImageView: UIImageView!
    
}
