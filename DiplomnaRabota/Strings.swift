//
//  Strings.swift
//  DiplomnaRabota
//
//  Created by Stanislav Georgiev on 2.05.18.
//  Copyright © 2018 Stanislav Georgiev. All rights reserved.
//

import Foundation

struct Strings {
    
    struct Buttons {
        static let logIn = "Влез"
        static let ok    = "Ok"
        static let reset = "Рестарт"
    }
    
    struct Placeholders {
        static let userName = "Потребителско име"
        static let password = "Парола"
    }
    
    struct Titles {
        static let loginController  = "Екран за вход"
        static let defaultTitle     = "Пъзел за деца"
        static let choosePuzzle     = "Избери пъзел"
    }
    
    struct Labels {
        static let rows    = "Редове"
        static let columns = "Колони"
        static let moves   = "Ходове:"
    }
    
    struct Keys {
        static let isLoggedIn = "isLoggedIn"
        static let username   = "username"
        static let password   = "password"
        static let userId     = "userId"
    }
    
    struct Messages  {
        static let wrongCredentials = "Няма такъв потребител."
        static let emptyTextFields  = "Моля попълнете всички задължителни полета."
        static let defaultMessage   = "Нещо се обърка. Моля опитайте пак."
    }
    
}
