//
//  ChoosePuzzleTableViewController.swift
//  DiplomnaRabota
//
//  Created by Stanislav Georgiev on 2.05.18.
//  Copyright © 2018 Stanislav Georgiev. All rights reserved.
//

import UIKit

class ChoosePuzzleTableViewController: UITableViewController {
    
    // MARK: Constants
    
    private var _puzzlePreviewCellIdentifier   = "puzzlePreviewCellIdentifier"
    private var _showPuzzleGameSegueIdentifier = "showPuzzleGame"


    // MARK: Variables
    
    private var puzzleArray  = [Puzzle]()
    private var selectedCell : Int!

    
    // MARK: Controller Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setData()
        self.applyTheme()
//        self.getPuzzlesFromDB()
        self.fillPuzzleArray()

    }
    
    // MARK: Private Methods
    
    private func setData() {
        self.navigationItem.title = Strings.Titles.choosePuzzle
    }
    
    private func applyTheme() {
        self.view.backgroundColor = UIColor(patternImage: #imageLiteral(resourceName: "blue"))
        self.tableView.tableFooterView = UIView()
    }
    
    private func getPuzzlesFromDB() {
        WebApiController.shared.getPuzzles(userId: 1) { (puzzles, error) in
            if puzzles != nil {
                print("Success")
            }
        }
    }
    
    
    private func fillPuzzleArray() {
        self.puzzleArray = [
                Puzzle(name: "zebra",   image: #imageLiteral(resourceName: "zebra"), rows: 3, columns: 3),
                Puzzle(name: "monkey",  image: #imageLiteral(resourceName: "monkey"), rows: 3, columns: 3),
                Puzzle(name: "shark",   image: #imageLiteral(resourceName: "shark"), rows: 4, columns: 4),
                Puzzle(name: "giraffe", image: #imageLiteral(resourceName: "giraffe"), rows: 3, columns: 3)
            ]
        
        //self.puzzleArray + arrayFromDB
    }
    
    
    // MARK: Helpers
    
    // MARK: Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let nextVC = segue.destination as? PuzzleViewController {
            nextVC.selectedPuzzle = self.puzzleArray[selectedCell]
        }
    }
    

    // MARK: - Table view data source
    

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.puzzleArray.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: _puzzlePreviewCellIdentifier, for: indexPath) as! PuzzlePreviewTableViewCell
        
        cell.setCellData(from: self.puzzleArray[indexPath.row])

        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.selectedCell = indexPath.row
        
        self.performSegue(withIdentifier: _showPuzzleGameSegueIdentifier, sender: self)
    }
 

}

