//
//  PuzzleViewController.swift
//  DiplomnaRabota
//
//  Created by Stanislav Georgiev on 20.03.18.
//  Copyright © 2018 Stanislav Georgiev. All rights reserved.
//

import UIKit

class PuzzleViewController: UIViewController {
    
    
    // MARK: Outlets
    
    @IBOutlet weak var puzzleCollectionView: UICollectionView!
    @IBOutlet weak var piecesCollectionView: UICollectionView!
    
    @IBOutlet weak var movesLabel: UILabel!
    
    @IBOutlet weak var resetPuzzleButton: UIButton!
    
    
    // MARK: Constants
    
    private let _puzzleCellIdentifier      = "puzzleCellIdentifier"
    private let _puzzlePieceCellIdentifier = "puzzlePieceCellIdentifier"

    
    // MARK: Variables
    
    private var piecesImageArray : [[PuzzleImage]] = [[]]
    private var puzzleImageArray : [[UIImage]]     = [[]]
    
    private var sourceIndexPath = IndexPath()
    
    private var gameDifficulty: Int = 3 // 3x3,4x4...
    
    private var numberOfMovesToComplete: Int = 0
    private var numberOfMoves: Int = 0 { // trqbva da e 0, sega e 1 zashtoto imam edin image zareden predvaritelno
        didSet {
            self.showNumberOfMoves()
        }
    }
    
    var selectedPuzzle: Puzzle!
    
    
    // MARK: Controller Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setData()
        self.customInit()
        self.applyTheme()
    }
    
    
    // MARK: Private Methods
    
    private func customInit() {
        self.puzzleCollectionView.delegate = self
        self.puzzleCollectionView.dataSource = self
        self.puzzleCollectionView.dropDelegate = self
        
        self.piecesCollectionView.delegate = self
        self.piecesCollectionView.dataSource = self
        self.piecesCollectionView.dragDelegate = self
        self.piecesCollectionView.dropDelegate = self
        self.piecesCollectionView.dragInteractionEnabled = true
        
        self.puzzleCollectionView.isScrollEnabled = false
        self.piecesCollectionView.isScrollEnabled = false
        
        self.showNumberOfMoves()
    }
    
    private func setData() {
        self.puzzleImageArray = [
            [UIImage(), UIImage(), UIImage()],
            [UIImage(), UIImage(), UIImage()],
            [UIImage(), UIImage(), UIImage()]
        ] // Extension na array
        
        if selectedPuzzle.id == nil {
            self.piecesImageArray = self.fillPuzzlePiecesArray(from: selectedPuzzle) // Extension na array
            self.gameDifficulty   = self.selectedPuzzle.rows
            
            return
        }
        
        self.showNumberOfMoves()
        self.navigationItem.title = Strings.Titles.choosePuzzle
        self.resetPuzzleButton.setTitle(Strings.Buttons.reset, for: .normal)
        
        //self.piecesImageArray = getPieces(for: selectedPuzzle.id!) // ot bazata
    }
    
    private func applyTheme() {
        self.puzzleCollectionView.layer.borderWidth = 1
        self.puzzleCollectionView.layer.borderColor = UIColor.black.cgColor
    }
    
    private func image(from multidimantionalImageArray:[[UIImage]], at indexPath: IndexPath) -> UIImage {
        return multidimantionalImageArray[indexPath.section][indexPath.row]
    }
    
    private func items(from multidimantionalImageArray:[[UIImage]], at section: Int) -> Int {
        return multidimantionalImageArray[section].count
    }
    
    private func fillPuzzlePiecesArray(from puzzle: Puzzle) -> [[PuzzleImage]] {
        var piecesArray: [[PuzzleImage]] = []
        
        let rows      = puzzle.rows
        let columns   = puzzle.columns
        let levelName = puzzle.name
        
        for row in 0..<rows {
            piecesArray.append([PuzzleImage]())
            
            for column in 0..<columns {
                piecesArray[row].append(PuzzleImage.fromFile(named: "\(levelName)", positioned: "\(row)\(column)")!)
            }
            
        }
        
        return piecesArray.shuffled()
    }
    
    private func checkIfPuzzleIsCompleted() {
        if self.numberOfMovesToComplete == gameDifficulty * gameDifficulty  { // da se opravi vzimaiki ot selectedPuzzle
            let alert =  UIAlertController(title: "Поздравления!", message: "Успя да наредиш пъзела!", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ок", style: .default, handler: {
                _ in self.resetPuzzle()
            }))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    private func resetPuzzle() -> Void {
        self.puzzleImageArray = [
            [UIImage(), UIImage(), UIImage()],
            [UIImage(), UIImage(), UIImage()],
            [UIImage(), UIImage(), UIImage()]
        ]
        self.numberOfMoves = 0
        self.numberOfMovesToComplete = 0
        if self.selectedPuzzle.id == nil {
           self.piecesImageArray = self.fillPuzzlePiecesArray(from: selectedPuzzle) // Extension na array
        } else {
            //self.piecesImageArray = getPieces(for: selectedPuzzle.id!, user: UserDefaults.userID, rows: selectedPuzzle.rows, columns: selectedPuzzle.columns) // ot bazata i da se napravi sushtoto kato v fillPuzzlePiecesArray, samo che vuv funkciqta ot bazata
        }
        
        self.puzzleCollectionView.reloadData()
        self.piecesCollectionView.reloadData()
    }
    
    private func showNumberOfMoves() {
        self.movesLabel.text = "\(Strings.Labels.moves) \(numberOfMoves)"
    }
    
    
    // MARK: Helpers
    
    
    // MARK: Actions
    
    @IBAction func resetPuzzle(_ sender: Any) {
        self.resetPuzzle()
    }
    
}

extension PuzzleViewController: UICollectionViewDelegate {
    
    // MARK: Collection View Delegate

}

extension PuzzleViewController: UICollectionViewDelegateFlowLayout {
    
    // MARK: Collection View Delegate Flow Layout
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let collectionViewWidth = collectionView.frame.width

        return CGSize(width: collectionViewWidth/CGFloat(gameDifficulty) - 3, height: collectionViewWidth/CGFloat(gameDifficulty) - 3)

    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 1, left: 3, bottom: 1, right: 3)

    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
}

extension PuzzleViewController: UICollectionViewDataSource {
    
    // MARK: Collection View Data Source
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return collectionView == self.piecesCollectionView ? self.piecesImageArray.count : self.puzzleImageArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return collectionView == self.piecesCollectionView ?
            self.items(from: piecesImageArray, at: section) :
            self.items(from: puzzleImageArray, at: section)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView === self.puzzleCollectionView {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: _puzzleCellIdentifier, for: indexPath) as! PuzzleImageCollectionViewCell

            cell.puzzlePieceImageView.image = self.image(from: self.puzzleImageArray, at: indexPath)
            return cell
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: _puzzlePieceCellIdentifier, for: indexPath) as! PuzzleImageCollectionViewCell

            cell.puzzlePieceImageView.image = self.image(from: self.piecesImageArray, at: indexPath)
            return cell
        }
    }

}

extension PuzzleViewController: UICollectionViewDragDelegate {
    
    // MARK: Collection View Drag Delegate
    
    func collectionView(_ collectionView: UICollectionView, itemsForBeginning session: UIDragSession, at indexPath: IndexPath) -> [UIDragItem] {
        if collectionView == self.piecesCollectionView {
            
            self.sourceIndexPath = indexPath // Sluji za zapametqvane na indexpatha na kartinkata koqto shte se mesti za da moga da q premahna sled izmestvaneto
            
            let cell = collectionView.cellForItem(at: indexPath) as! PuzzleImageCollectionViewCell
            let photo = cell.puzzlePieceImageView.image
            let imageProvider = NSItemProvider(object: photo ?? UIImage())
            let dragItem = UIDragItem(itemProvider: imageProvider)
            dragItem.localObject = photo
            return [dragItem]
        }
        
        return []
    }
}

extension PuzzleViewController: UICollectionViewDropDelegate {
    
    // MARK: Collection View Drop Delegate
    
    func collectionView(_ collectionView: UICollectionView, performDropWith coordinator: UICollectionViewDropCoordinator) {
        guard let destinationIndexPath = coordinator.destinationIndexPath else {
            print("No destination path.")
            return
        }
        
        guard let item = coordinator.items.first else {
            print("No item to be moved.")
            return
        }
        
        self.numberOfMoves += 1
        
        let currentPosition  = "\(destinationIndexPath.section)\(destinationIndexPath.row)"
        
        let puzzlePieceImage = item.dragItem.localObject as! PuzzleImage
        
        let puzzlePiecePlace = puzzlePieceImage.position
        
        if currentPosition != puzzlePiecePlace {
            return
        }
        
        collectionView.performBatchUpdates({
            if collectionView == self.puzzleCollectionView {
                self.puzzleImageArray[destinationIndexPath.section][destinationIndexPath.row] = item.dragItem.localObject as! UIImage
            }
            self.piecesImageArray[sourceIndexPath.section].remove(at: sourceIndexPath.row)
            self.piecesCollectionView.deleteItems(at: [self.sourceIndexPath])
            self.piecesCollectionView.reloadItems(at: self.piecesCollectionView.indexPathsForVisibleItems)
            collectionView.deleteItems(at: [destinationIndexPath]) // Premahvane na UIImage() ot destinationPath-a
            collectionView.insertItems(at: [destinationIndexPath])
            })
        
        coordinator.drop((item.dragItem), toItemAt: destinationIndexPath)
        
        numberOfMovesToComplete += 1
        
        self.checkIfPuzzleIsCompleted()
    }
    
    func collectionView(_ collectionView: UICollectionView, dropSessionDidUpdate session: UIDropSession, withDestinationIndexPath destinationIndexPath: IndexPath?) -> UICollectionViewDropProposal {
        
        if session.localDragSession != nil {
            
            if collectionView.hasActiveDrag {
                return UICollectionViewDropProposal(operation: .forbidden, intent: .unspecified) // zabranqva mesteneto na Image v sushtoto collectionview
            } else {
                return UICollectionViewDropProposal(operation: .move, intent: .insertIntoDestinationIndexPath)
            }
        }
        return UICollectionViewDropProposal(operation: .forbidden)
    }

}



