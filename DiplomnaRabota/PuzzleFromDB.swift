//
//  PuzzleFromDB.swift
//  DiplomnaRabota
//
//  Created by Guest User on 26/08/2018.
//  Copyright © 2018 Stanislav Georgiev. All rights reserved.
//

import UIKit

class PuzzleFromDB: NSObject, Codable {
    private(set) var id: Int
    private(set) var puzzleName: String
    private(set) var puzzleImage: String
    private(set) var difficulty: String
    
    init(id: Int, name: String, image: String, difficulty: String) {
        self.id = id
        self.puzzleName = name
        self.puzzleImage = image
        self.difficulty = difficulty
    }
}
