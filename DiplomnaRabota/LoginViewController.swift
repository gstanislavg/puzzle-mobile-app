//
//  LoginViewController.swift
//  DiplomnaRabota
//
//  Created by Stanislav Georgiev on 2.05.18.
//  Copyright © 2018 Stanislav Georgiev. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {
    
    // MARK: Enums
    
    enum LoginErrors: Int {
        case wrongCredentials
        case emptyFields
    }
    
    
    // MARK: Outlets
    
    @IBOutlet weak var userNameTextField : UITextField!
    @IBOutlet weak var passwordTextField : UITextField!
    
    @IBOutlet weak var loginButton       : UIButton!
    
    @IBOutlet weak var logoImageView     : UIImageView!
    
    @IBOutlet weak var passwordTextFieldBottomConstraint: NSLayoutConstraint!
    
    // MARK: Constants
    
    let defaults = UserDefaults.standard
    let _showPuzzleListSegueIdentifier = "showPuzzleList"
    
    
    // MARK: Variables
    
    var loadingHud = UIView()
    
    static var pediatricId: Int?
    
    
    // MARK: Controller Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setData()
        self.customInit()
        self.applyTheme()
        self.checkUser()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true) // pri natiskane na mqsto ot ekrana se skriva klaviaturata
    }
    
    
    // MARK: Private Methods
    
    private func checkUser() {
        if isLoggedIn {
            self.goToChoosePuzzle()
        }
    }
    
    private func customInit() {
        
    }
    
    private func setData() {
        self.userNameTextField.placeholder = Strings.Placeholders.userName
        self.passwordTextField.placeholder = Strings.Placeholders.password
        
        self.loginButton.setTitle(Strings.Buttons.logIn   , for: .normal)
        
        self.userNameTextField.text = defaults.object(forKey: .username) as? String ?? ""
        self.passwordTextField.text = defaults.object(forKey: .password) as? String ?? ""
        
        self.navigationItem.title = Strings.Titles.loginController
        
        self.logoImageView.image  = #imageLiteral(resourceName: "puzzleLogo")
    }
    
    private func applyTheme() {
        self.setBackgroundImage()
    }
    
    private func setBackgroundImage() {
        let backgroundImage = UIImage.init(named: "background1")
        let backgroundImageView = UIImageView.init(frame: self.view.frame)
        
        backgroundImageView.image = backgroundImage
        backgroundImageView.contentMode = .scaleAspectFit
        backgroundImageView.alpha = 0.8
        
        self.view.insertSubview(backgroundImageView, at: 0)
    }
    
    private func userExists() -> Bool {
        let userId = Services.sharedInstance.checkUser(userName: self.userNameTextField.safeText, password: self.passwordTextField.safeText)
        
        if userId != 0 {
            defaults.set(userId, forKey: .userId)
            return true
        }

        return false
    }
    
    private func goToChoosePuzzle() {
        self.performSegue(withIdentifier: _showPuzzleListSegueIdentifier, sender: self)
    }
    
    private func showAlertController(for error: LoginErrors) { // да се направи екстенсион на UIAlertController
        let message = self.errorMessage(for: error)
        let title   = Strings.Titles.defaultTitle
        
        let alert   = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction (title: Strings.Buttons.ok, style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    private func errorMessage(for error: LoginErrors) -> String {
        switch error {
        case .wrongCredentials:
            return Strings.Messages.wrongCredentials
        case .emptyFields:
            return Strings.Messages.emptyTextFields
        }
    }
    
    private func showLoadingHud() {
        loadingHud = UIViewController.displaySpinner(onView: self.view)
    }
    
    private func hideLoadingHud() {
        UIViewController.removeSpinner(spinner: loadingHud)
    }
    
    
    // MARK: Helpers
 
    var isLoggedIn: Bool {
        return defaults.bool(forKey: .isLoggedIn)
    }
    
    var isFormValid: Bool {
        return !self.userNameTextField.safeText.isEmpty ||
               !self.passwordTextField.safeText.isEmpty
    }

    
    // MARK: Actions
    
    @IBAction func logIn(_ sender: Any) {
//        if !self.isFormValid {
//            self.showAlertController(for: .emptyFields)
//            return
//        }
//        
//        if !self.userExists() {
//            self.showAlertController(for: .wrongCredentials)
//            return
//        }
//        WebApiController.shared.checkUser(user: UserModel(id: 0, name: self.userNameTextField.text!, password: self.passwordTextField.text!, pediatricId: 0), completion: {user, error in
//            if user != nil {
//                LoginViewController.pediatricId = user?.pediatricId
//                self.goToChoosePuzzle()
//            }
//        })
        self.goToChoosePuzzle()
    }
    
}

extension LoginViewController: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.moveTextField(textField, moveDistance: -110, up: true)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.moveTextField(textField, moveDistance: -110, up: false)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == self.userNameTextField {
            self.passwordTextField.becomeFirstResponder()
        }
        
        if textField == self.passwordTextField {
            self.logIn(self)
        }
        return true
    }
    
    func moveTextField(_ textField: UITextField, moveDistance: Int, up: Bool) {
        let moveDuration = 0.3
        let movement: CGFloat = CGFloat(up ? moveDistance : -moveDistance)
        
        UIView.beginAnimations("animateTextField", context: nil)
        UIView.setAnimationBeginsFromCurrentState(true)
        UIView.setAnimationDuration(moveDuration)
        self.view.frame = self.view.frame.offsetBy(dx: 0, dy: movement)
        UIView.commitAnimations()
    }
}
