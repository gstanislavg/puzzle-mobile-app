//
//  WebApiController.swift
//  DiplomnaRabota
//
//  Created by Guest User on 26/08/2018.
//  Copyright © 2018 Stanislav Georgiev. All rights reserved.
//

import UIKit

class WebApiController: NSObject {
    
    static let shared = WebApiController()
    
    func checkUser(user: UserModel, completion:@escaping ((_ user: UserModel?, _ error: Error?) -> Void)) {
        var urlComponents = URLComponents()
        urlComponents.scheme = "http"
        urlComponents.host = "192.168.0.104:2084"
        urlComponents.path = "//userExists"
        guard let url = urlComponents.url else { fatalError("Could not create URL from components") }

        var request = URLRequest(url: url)
        request.httpMethod = "POST"

        var headers = request.allHTTPHeaderFields ?? [:]
        headers["Content-Type"] = "application/json"
        request.allHTTPHeaderFields = headers

        let encoder = JSONEncoder()
        do {
            let jsonData = try encoder.encode(user)
            request.httpBody = jsonData
            print("jsonData: ", String(data: request.httpBody!, encoding: .utf8) ?? "no body data")
        } catch {
            completion(nil, error)
        }
        
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        let task = session.dataTask(with: request) { (responseData, response, responseError) in
            let decoder = JSONDecoder()
            
            do {
                let user = try decoder.decode(UserModel.self, from: responseData!)
                completion(user, nil)
            } catch {
                completion(nil, error)
            }
        }
        task.resume()
    }
    
    func getPuzzles(userId: Int, completion:@escaping ((_ puzzles: [PuzzleFromDB]?, _ error: Error?) -> Void)) {
        var urlComponents = URLComponents()
        urlComponents.scheme = "http"
        urlComponents.host = "192.168.0.104:2084"
        urlComponents.path = "/puzzleForPediatric/\(userId)"
//        let url = URL(string: "192.168.0.104:2084/puzzleForPediatric/1")
        guard let url = urlComponents.url else { fatalError("Could not create URL from components") }

        var request = URLRequest(url: url)
        request.httpMethod = "GET"

        
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)

        let task = session.dataTask(with: request) { (responseData, response, responseError) in
            let decoder = JSONDecoder()

            do {
                let puzzles = try decoder.decode([PuzzleFromDB].self, from: responseData!)
                completion(puzzles, nil)
            } catch {
                completion(nil, error)
            }
        }
        task.resume()
    }
}
