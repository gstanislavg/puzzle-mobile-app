//
//  UserModel.swift
//  DiplomnaRabota
//
//  Created by Guest User on 26/08/2018.
//  Copyright © 2018 Stanislav Georgiev. All rights reserved.
//

import UIKit

class UserModel: NSObject, Codable {
    private(set) var id: Int
    private(set) var uName: String
    private(set) var uPassword: String
    private(set) var pediatricId: Int
    
    init(id: Int, name: String, password: String, pediatricId: Int) {
        self.id = id
        self.uName = name
        self.uPassword = password
        self.pediatricId = pediatricId
    }
}
