//
//  String Helpers.swift
//  DiplomnaRabota
//
//  Created by Stanislav Georgiev on 3.05.18.
//  Copyright © 2018 Stanislav Georgiev. All rights reserved.
//

import UIKit

extension String {
    static let isLoggedIn = Strings.Keys.isLoggedIn
    static let username   = Strings.Keys.username
    static let password   = Strings.Keys.password
    static let userId     = Strings.Keys.userId
}
